# -*- coding: utf-8 -*-
# Copyright (c) 2015, Revant Nandgaonkar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class GSPUser(Document):
	def validate(self):
		self.set_state_code()

	def set_state_code(self):
		if not self.state_code:
			self.state_code = frappe.db.get_value("GST India State", self.address_state, "number")