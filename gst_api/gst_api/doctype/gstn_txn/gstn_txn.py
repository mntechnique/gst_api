# -*- coding: utf-8 -*-
# Copyright (c) 2015, Revant Nandgaonkar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, random
from frappe.model.document import Document

class GSTNTXN(Document):
	def validate(self):
		self.txn = self.name
	
	def autoname(self):
		self.name = "TXN" + str(frappe.generate_hash(length=11)).upper() # "11111111111"
