// Copyright (c) 2016, Revant Nandgaonkar and contributors
// For license information, please see license.txt

frappe.ui.form.on('GST API Settings', {
	refresh: function(frm) {
		frm.add_custom_button(__('Generate App Key'), function(){
			if (frm.doc.app_key) {
				frappe.confirm("The current key will be replaced. Proceed?",
					function() {
						generate_and_set_key(frm);
					}
				);
			} else {
				generate_and_set_key(frm);
			}
		});
	}
});

function generate_and_set_key(frm){
	frappe.call({
		method: "gst_api.gst_api.doctype.gst_api_settings.gst_api_settings.generate_app_key",
		freeze: true,
		freeze_message: __("Generating App key"),
		callback: function(r){
			if(!r.exc) {
				frm.set_value("app_key", r.message);
			} else {
				frappe.msgprint(__("Key was not generated. <br /> " + r.exc));
			}
		}
	});
}
