import os
import base64
import random
import hashlib

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import (
    Cipher, algorithms, modes
)
from cryptography.hazmat.primitives import padding

from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto.Util.asn1 import DerSequence
from binascii import a2b_base64

class AesCrypt256:
    """
    Aes Crypter based on pyCrypto
    will replace Lib/Norris/AesCrypter.py
    >>> c = AesCrypt256()
    >>> key = 'mysecret'
    >>> text = 'foobar'
    >>> c.decrypt(key,c.encrypt(key,text))
    'foobar'
    >>> c.decryptB64(key,c.encryptB64(key,text))
    'foobar'
    
    >>> c.pkcs5_unpad(c.pkcs5_pad('foobar'))
    'foobar'
    >>> c.pkcs5_unpad(c.pkcs5_pad('foobar-'*10))
    'foobar-foobar-foobar-foobar-foobar-foobar-foobar-foobar-foobar-foobar-'
    """
    
    BLOCK_SIZE = 32

    def pkcs5_pad(self,s):
        """
        padding to blocksize according to PKCS #5
        calculates the number of missing chars to BLOCK_SIZE and pads with
        ord(number of missing chars)
        @see: http://www.di-mgt.com.au/cryptopad.html
        @param s: string to pad
        @type s: string
        @rtype: string
        """
        return s + (self.BLOCK_SIZE - len(s) % self.BLOCK_SIZE) * chr(self.BLOCK_SIZE - len(s) % self.BLOCK_SIZE)

    def pkcs5_unpad(self,s):
        """
        unpadding according to PKCS #5
        @param s: string to unpad
        @type s: string
        @rtype: string
        """
        return s[0:-ord(s[-1])]


    def encrypt(self, key, value):
        """Encrypt value by key
        @param key: key to encrypt with
        @type key: string
        @param value: value to encrypt
        @type value: string
        @rtype: string
        """

        #iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
        key = hashlib.sha256(key).digest()[:self.BLOCK_SIZE]
        #cipher = AES.new(key, AES.MODE_CBC, iv)
        cipher = AES.new(key, AES.MODE_ECB)
        crypted = cipher.encrypt(self.pkcs5_pad(value))
        #return iv+crypted
        return crypted


    def decrypt(self, key, value):
        """Decrypt value by key
        @param key: key to decrypt with
        @type key: string
        @param value: value to decrypt
        @type value: string
        @rtype: string
        """
        key = hashlib.sha256(key).digest()[:self.BLOCK_SIZE]
        #iv = value[:16]
        crypted = value[16:]
        #cipher = AES.new(key,AES.MODE_CBC,iv)
        cipher = AES.new(key,AES.MODE_ECB)
        return self.pkcs5_unpad(cipher.decrypt(crypted))

    def encryptB64(self, key, value):
        """Encrypt and return in base64
        @param key: key to encrypot with
        @type key: string
        @param value: value to encrypt
        @type value: string
        @rtype: string
        """
        return base64.b64encode(self.encrypt(key, value))

    def decryptB64(self, key, value):
        """decrypt from base64
        @param key: key to decrypt with
        @type key: string
        @param value: value to decrypt in base64
        @type value: string
        @rtype: string
        """        
        return self.decrypt(key,base64.b64decode(value))

class AESCryptographyIO:
    def __init__(self, key):
        self.key = key

    # def encrypt(self, plaintext, associated_data):
    def encrypt(self, plaintext):
        key = self.key

        # Construct an AES-ECB Cipher object with the given key and a
        # randomly generated IV.
        encryptor = Cipher(
            algorithms.AES(key),
            modes.ECB(),
            backend=default_backend()
        ).encryptor()

        # associated_data will be authenticated but not encrypted,
        # it must also be passed in on decryption.
        # encryptor.authenticate_additional_data(associated_data)

        # Encrypt the plaintext and get the associated ciphertext.
        # GCM does not require padding.
        ciphertext = encryptor.update(plaintext) + encryptor.finalize()

        # return (ciphertext, encryptor.tag)
        return ciphertext
    
    # def decrypt(self, associated_data, ciphertext):
    def decrypt(self, ciphertext):
        # Construct a Cipher object, with the key
        key = self.key
        decryptor = Cipher(
            algorithms.AES(key),
            modes.ECB(),
            backend=default_backend()
        ).decryptor()

        # We put associated_data back in or the tag will fail to verify
        # when we finalize the decryptor.
        # decryptor.authenticate_additional_data(associated_data)

        # Decryption gets us the authenticated plaintext.
        # If the tag does not match an InvalidTag exception will be raised.
        return decryptor.update(ciphertext) + decryptor.finalize()

    def pad(self, data):
        padder = padding.PKCS7(64).padder()
        padded_data = padder.update(bytes(data))
        padded_data += padder.finalize()
        return padded_data

    def unpad(self, padded_data):
        unpadder = padding.PKCS7(64).unpadder()
        data = unpadder.update(padded_data)
        data += unpadder.finalize()
        return data

    def encryptb64(self, value):
        return base64.b64encode(self.encrypt(value))
    
    def decryptb64(self, value):
        return self.decrypt(base64.b64decode(value))

def encrypt_aes_ecb_pkcs5(text = 'to be encrypted'):
    BS = AES.block_size
    pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
    unpad = lambda s : s[0:-ord(s[-1])]

    #key = os.urandom(32) # the length can be (16, 24, 32)
    pem = open(getattr(frappe.local, "site_path", None) + "/GSTN_PublicKey.pem").read()
    print pem
    # lines = pem.replace(" ",'').split()
    # der = a2b_base64(''.join(lines[1:-1]))
    # cert = DerSequence()
    # cert.decode(der)
    # tbsCertificate = DerSequence()
    # tbsCertificate.decode(cert[0])
    # print type(tbsCertificate) #subjectPublicKeyInfo = tbsCertificate[6]

    # Initialize RSA key
    rsa_key = RSA.importKey(pem)
    print rsa_key

    cipher = AES.new(os.urandom(32))

    encrypted = cipher.encrypt(pad(text)).encode('hex')
    print encrypted  # will be something like 'f456a6b0e54e35f2711a9fa078a76d16'

    decrypted = unpad(cipher.decrypt(encrypted.decode('hex')))
    print decrypted  # will be 'to be encrypted'